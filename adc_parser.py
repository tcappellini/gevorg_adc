#!/usr/bin/python

import argparse
import base64
import json
import struct
import traceback


def parse_data(raw_data, encode_data):
    """
    Parse the ADC data so that 8 channels of data comprise 1 sample.
    Return a dictionary where each sample # is a dictionary key.

    :param raw_data:
    :param encode_data:
    :return adc_samples_dict:
    """
    bytes_per_sample = 32
    total_samples = len(raw_data) / bytes_per_sample  # each sample is 32 bytes, there are 8 4-byte ADC readings for each sample
    number_of_channels = 8
    bytes_per_adc_reading = 4

    # creates an empty ist for each channel #. The ADC readings for each channel will be stored in the lists
    adc_data_dict = {'CH {:03d}'.format(channel_id): [] for channel_id in range(0, number_of_channels)}
    adc_samples_dict = {}

    for sample_num in range(0, total_samples):
        start = sample_num * bytes_per_sample
        stop = start + bytes_per_adc_reading
        pad = '   '

        # iterate through all 8 channels
        for channel_count in range(0, number_of_channels):
            ADC_Ubyte1, ADC_Ubyte2, ADC_Ubyte3, Uheader = struct.unpack('<BBBB', raw_data[start:stop])

            full_adc = struct.unpack('<i', raw_data[start:stop - 1] + ('\0' if raw_data[start + 2] < '\x80' else '\xff'))[0]

            if encode_data:
                # base64 encoding for the ADC readings
                adc_data = base64.b64encode('{:02X}   {:02X}  {:02X}  {:<2X} {:<8d}'.format(ADC_Ubyte3, ADC_Ubyte2, ADC_Ubyte1, Uheader, full_adc))
            else:
                # for debugging - the user can look at the JSON data to check the unencoded values
                adc_data = '{:02X}   {:02X}  {:02X}  {:<2X} {:<8d}'.format(ADC_Ubyte3, ADC_Ubyte2, ADC_Ubyte1, Uheader, full_adc)

            channel_str = 'CH {:03d}'.format(Uheader & 0x7)  # the channel ID is stored in the low 3 bits of the header
            adc_data_dict[channel_str].append(adc_data)

            # offset to next channel's data
            start += bytes_per_adc_reading
            stop = start + bytes_per_adc_reading

        adc_samples_dict['Sample # {:03d}'.format(sample_num)] = adc_data_dict

    # add the total number of samples, so that they will be displayed at the top of the JSON file
    adc_samples_dict['000_sample_count'] = total_samples

    return adc_samples_dict


def read_input_file(filename):
    """
    Open the file, read the data and return it as a string
    The file is automatically closed

    :param filename:
    :return:
    """
    with open(filename, 'rb') as fh_in:
        data = fh_in.read()
    return data


def save_output_file(data_dict, filename):
    """
    Write the data_dict to the file specified in filename, to a JSON file.
    The ADC data is BASE64 encoded.

    :param data_dict:
    :param filename:
    :return:
    """
    with open(filename, 'w') as output_file:
        # save the ADC samples so that all 32 readings for each channel are saved together, for Each sample number
        json.dump(data_dict, output_file, sort_keys=True, indent=4, separators=(':', ','))


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('data_file', help='The filename containing the ADC data.')
    parser.add_argument('-r', '--raw', action='store_false', help="The ADC samples will not be Base64 encoded. For debugging the ADCs.")

    parse_args = parser.parse_args()
    data_file = parse_args.data_file

    try:
        raw_data = read_input_file(data_file)
        print('\n\n\t{} is {} bytes long'.format(data_file, len(raw_data)))

        if raw_data:
            data_dict = parse_data(raw_data, parse_args.raw)
            if data_dict:
                json_filename = '{}.json'.format(data_file.split('.')[0])
                save_output_file(data_dict, json_filename)

                encoding_msg = 'BASE64-encoded'
                if not parse_args.raw:
                    encoding_msg = 'Raw'
                print('\n\t{} data was written to {}.json.\n\n'.format(encoding_msg, data_file.split('.')[0]))
        else:
            print('Could not read data from {}\n\n'.format(data_file))

    except KeyboardInterrupt:
        print('\n\n\tUSER ABORT\n\n')
    except IOError as exc:
        print('\nERROR: {} - {}\n\n'.format(exc.strerror, exc.filename))
    except Exception:
        print('\nUNEXPECTED EXCEPTION: {}\n'.format(traceback.format_exc()))


if __name__ == '__main__':
    main()
