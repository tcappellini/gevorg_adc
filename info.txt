Data is divided into 4-byte words
Each 4-byte word corresponds to an ADC sample from a single ADC channel.
Here is how data is structured [CH0_sample],[CH1_ sample ]...[CH7_ sample ],[CH0_data]
Below is the structure of the 4-byte sample word
Byte 0 -> ADC Data Byte 3
Byte 1 -> ADC Data Byte 2
Byte 2 -> ADC Data Byte 1
Byte 3 -> Header
First 3 bits of the header are the channel ID
Below is the example data capture for eight ADC channels. Bytes in the rectangles are the header bytes for each sample taken for ADC channels 0-7
image.png
The idea is to parse the raw data into a structure so it can be encoded to a JSON stream which would look like this:

“sensors”: {
          "sample_count": 32
          “0": {
            “data”: { "Base64 encoded data for CH0 for 32 samples" }
          }
          “1": {
            “data”: { " Base64 encoded data for CH1 for 32 samples " }
          }
          “2": {
            “data”: { "Base64 encoded data for CH2 for 32 samples " }
          }
         {...}
}

I think it might make sense to turn 24-bit data into 32-bit int and send the array of 32-bit integers as a raw data..
